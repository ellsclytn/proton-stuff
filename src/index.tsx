import * as React from 'react'
import { render, Window, App, Box, Form, TextInput } from 'proton-native'

const MyApp = () => (
  <App>
    <Window
      title='Proton Native Rocks!'
      size={{ w: 500, h: 500 }}
      menuBar={false}
    >
      <Box padded>
        <Form padded stretchy={false}>
          <TextInput label='Username' />
          <TextInput label='Password' />
        </Form>
      </Box>
    </Window>
  </App>
)

render(<MyApp />)
